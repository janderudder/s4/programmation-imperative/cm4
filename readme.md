# programmation impérative

## CM 4

L'utilisation de l'en-tête standard `math.h` nécessite de lier avec la bibliothèque `libm` (option `-lm` pour l'éditeur de liens).

    ./compile-run.sh ./src/07-math.c -lm
