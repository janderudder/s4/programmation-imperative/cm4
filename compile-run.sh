#!/bin/bash
#
#	Compile (to ./bin/) and run a C source file.
#
[[ -d ./bin/ ]] || mkdir 'bin'

source="${1-src/main.c}"			# src/main.c if no argument provided
output="$(basename "$source")"
output="${output::-2}" 				# remove .c extension

shift
CompilerOptions="-std=c17 -Wall -Wextra"
LinkerOptions=''

# add args until '--' to either compiler or linker options
while [[ $1 && $1 != "--" ]]
do
	if [[ $1 =~ -l.+ ]]; then
		LinkerOptions+=" $1"
	else
		CompilerOptions+=" $1"
	fi
	shift
done

shift # pass args after '--' to the program

$CC $CompilerOptions -o bin/"$output" "$source" $LinkerOptions \
	&& ./bin/"$output" "$@"
