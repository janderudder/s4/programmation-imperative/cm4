#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

/**
 *  Voir source sur Moodle pour la version du prof.
 *
 *  Le but est de faire un tableau de pointeurs de fonction,
 *  qui seront ensuite appelées à la suite lors d'un appel
 *  à une fonction générale (call). L'appel à call supprime
 *  les pointeurs du tableau.
 */
typedef void (*vfn_ptr_t)();
typedef int (*ifni_ptr_t)(int);


int call_fn(ifni_ptr_t fn, int arg)
{
    return fn(arg);
}


int some_ifni(int arg)
{
    return 2*arg;
}


int main(void)
{
    printf("%d\n", some_ifni(12));
}
