#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>


int compare_int(const void* pLeft, const void* pRight)
{
    const int left = *(int*)pLeft;
    const int right = *(int*)pRight;
    return left - right;
}


int main(void)
{
    int arr[] = { 5, 1, 6, 7, 2, 3, 4, 9, 0, 8 };

    // /!\ PRECONDITION: bsearch needs a sorted array
    qsort(arr, sizeof arr/sizeof *arr, sizeof *arr, compare_int);

    const int key = 6;

    const int* found =
        bsearch(&key, arr, sizeof arr/sizeof *arr, sizeof *arr, compare_int);

    if (found) {
        printf("%d is at %p\n", *found, found);
        printf("arr[6] (value: %d) is at %p\n", arr[6], &arr[6]);
    } else {
        printf("not found\n");
    }

    return EXIT_SUCCESS;
}
