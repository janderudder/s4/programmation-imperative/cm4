#include <stdlib.h>
#include <stdio.h>


/* On précise le nombre de bit alloué à chaque champ. */
typedef struct _BitField
{
    int a: 3, b: 4;
    unsigned int c: 15;
    short d: 11;

} BitField;


typedef struct _BitField2
{
    char a: 3;

} BitField2;


typedef struct Int3 {
    int value: 3;
} Int3;



int main(void)
{
    BitField value = {.a=2, .b=5, .c=-1, .d=1};

    printf("sizeof value: %lu\n", sizeof value);
    printf("sizeof (int): %lu\n", sizeof(int));

    BitField value2 = {.a=2};
    printf("sizeof value2: %lu\n", sizeof value2);
    printf("sizeof (char): %lu\n", sizeof(char));

    Int3 i3 = { .value = 0b111 };
    printf("i3 value: %d\n", i3.value);

    return EXIT_SUCCESS;
}
