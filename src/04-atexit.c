#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>


#ifdef NDEBUG
#undef NDEBUG
#include <assert.h>
#endif


void on_exit(void)
{
    printf("Called by atexit.\n");
}


void on_quick_exit(void)
{
    printf("Program did quick exit.\n");
}


int main(const int argc, char** argv)
{
    printf("In main()\n");
    atexit(on_exit);
    printf("In main()\n");
    atexit(on_exit);
    printf("In main()\n");
    atexit(on_exit);
    printf("In main()\n");
    atexit(on_exit);

    at_quick_exit(on_quick_exit);

    if (argc>1 && !strcmp(argv[1], "fail")) {
        assert(0);
    }
    else if (argc>1 && !strcmp(argv[1], "quick")) {
        quick_exit(0);
    }

    return EXIT_SUCCESS;
}
