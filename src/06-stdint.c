#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    printf("sizeof(int8_t): %lu\n", sizeof(int8_t));
    printf("sizeof(int16_t): %lu\n", sizeof(int16_t));
    printf("sizeof(uint32_t): %lu\n", sizeof(uint32_t));
    printf("sizeof(uint64_t): %lu\n", sizeof(uint64_t));

    printf("\n");
    printf("sizeof(int_fast8_t): %lu\n", sizeof(int_fast8_t));
    printf("sizeof(int_fast16_t): %lu\n", sizeof(int_fast16_t));
    printf("sizeof(uint_fast32_t): %lu\n", sizeof(uint_fast32_t));
    printf("sizeof(uint_fast64_t): %lu\n", sizeof(uint_fast64_t));

    printf("\n");
    printf("sizeof(int_least8_t): %lu\n", sizeof(int_least8_t));
    printf("sizeof(int_least16_t): %lu\n", sizeof(int_least16_t));
    printf("sizeof(uint_least32_t): %lu\n", sizeof(uint_least32_t));
    printf("sizeof(uint_least64_t): %lu\n", sizeof(uint_least64_t));

    return EXIT_SUCCESS;
}
