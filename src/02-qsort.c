#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>


int compare_int(const void* pLeft, const void* pRight)
{
    const int left = *(int*)pLeft;
    const int right = *(int*)pRight;
    return left - right;
}


int main(void)
{
    int arr[] = { 5, 1, 6, 7, 2, 3, 4, 9, 0, 8 };

    qsort(arr, sizeof arr/sizeof *arr, sizeof *arr, compare_int);

    for (unsigned int i=0; i < sizeof arr / sizeof *arr; ++i) {
        printf("%d\n", arr[i]);
    }

    return EXIT_SUCCESS;
}
